<?php
namespace Controllers;

use Controllers\Interfaces\FileProcessorInterface;
use Exception;
use Reader\FileCreator;
use Reader\FileOutputter;

use Processor\FileProcessor;

class FileProcessorController implements FileProcessorInterface
{
    private $baseDir;
    private $fileCreator;
    private $fileOutputter;

    /**
     * FileProcessorController constructor.
     * @param FileCreator $fileCreator
     * @param FileOutputter $fileOutputter
     */
    public function __construct(FileCreator $fileCreator, FileOutputter $fileOutputter)
    {
        $this->baseDir       = __DIR__;
        $this->fileCreator   = $fileCreator;
        $this->fileOutputter = $fileOutputter;
    }

    /**
     * @param string $input
     * @throws Exception
     */
    public function processFile(string $input)
    {
        if (strlen($input) < 1) {
            throw new Exception('Invalid Filename.');
        }

        $isInDir    = file_exists($this->baseDir.DIRECTORY_SEPARATOR.$input);
        $isNotInDir = file_exists($input);

        if ($isInDir) {
            $file = $this->baseDir.DIRECTORY_SEPARATOR.$input;
        } elseif ($isNotInDir) {
            $file = $input;
        } else {
            throw new Exception('File Not Found.');
        }

        $sourceFile = $this->fileCreator->validateFile($file);
        $errorFile  = $this->fileCreator->createErrorFile($file);
        $outputFile = $this->fileCreator->createOutputFile($file);

        $this->generateOutput(new FileProcessor($sourceFile, "r"), $errorFile, $outputFile);
    }

    /**
     * @param FileProcessor $processor
     * @param string $errorFile
     * @param string $outputFile
     * @throws Exception
     */
    public function generateOutput(FileProcessor $processor, string $errorFile, string $outputFile)
    {
        $output  = [];
        $errors  = [];
        $pointer = 0;
        foreach ($processor->iterateLines() as $line) {
            $pointer++;
            if ($pointer == '1') {
                $header = $line;
                array_shift($header);
                $neededLineElements = count($header);
                continue;
            }

            // ignore empty lines
            if (!isset($line[0]) || is_null($line[0])) {
                continue;
            }

            $lineElements = count($line);
            if ($neededLineElements != $lineElements) {
                $errors[] = 'Expecting 18 fields but received '.$lineElements.' on line '.$pointer;
                continue;
            }

            if (!strtotime($line[self::DATE_POS])) {
                $errors[] = 'Invalid date received :'.$line[self::DATE_POS].' on line '.$pointer;
                continue;
            }
            $date = $line[self::DATE_POS];

            if (!strtotime($line[self::TIME_POS])) {
                $errors[] = 'Invalid time received :'.$line[self::TIME_POS].' on line '.$pointer;
                continue;
            }
            $time = $this->roundDownToMinuteInterval(new \DateTime($line[self::TIME_POS]));

            $rawStatus = $this->getStatus($line[self::STATUS_POS]);
            if (!$rawStatus) {
                $errors[] = 'Invalid Cache Status in :'.$line[self::STATUS_POS].' on line '.$pointer;
                continue;
            }
            $tcp  = $rawStatus['tcp'];
            $code = $rawStatus['code'];

            if (!ctype_digit($line[self::BYTES_POS])) {
                $errors[] = 'Invalid Bytes Number in :'.$line[self::BYTES_POS].' on line '.$pointer;
                continue;
            }
            $bytes = $line[self::BYTES_POS];

            $url = parse_url($line[self::HOST_POS]);
            if (!isset($url['host'])) {
                $errors[] = 'Invalid Host Name in :'.$line[self::HOST_POS].' on line '.$pointer;
                continue;
            }
            $host = $url['host'];

            if (!isset($output[$host])) {
                $output[$host] = [];
            }
            if (!isset($output[$host][$date])) {
                $output[$host][$date] = [];
            }
            if (!isset($output[$host][$date][$time])) {
                $output[$host][$date][$time] = [];
            }
            if (!isset($output[$host][$date][$time][$code])) {
                $output[$host][$date][$time][$code] = [];
            }
            if (!isset($output[$host][$date][$time][$code][$tcp])) {
                $output[$host][$date][$time][$code][$tcp] = ['requests' => 1, 'bytes' => intval($bytes)];
            } else {
                $output[$host][$date][$time][$code][$tcp]['requests']++;
                $output[$host][$date][$time][$code][$tcp]['bytes'] += intval($bytes);
            }
        }

        $this->setOutput($output, $outputFile, $errors, $errorFile);
    }

    /**
     * @param array $output
     * @param string $outputFile
     * @param array $errors
     * @param string $errorFile
     */
    public function setOutput(array $output, string $outputFile, array $errors, string $errorFile)
    {
        if (count($output)) {
            $this->fileOutputter->setOutputFile($outputFile, $output);
        }
        if (count($errors)) {
            $this->fileOutputter->setErrorFile($errorFile, $errors);
        }

        echo 'Done.'.PHP_EOL;
    }

    /**
     * @param \DateTime $dateTime
     * @param int $minuteInterval
     * @return string
     */
    public function roundDownToMinuteInterval(\DateTime $dateTime, $minuteInterval = 10)
    {
        return $dateTime->setTime(
            $dateTime->format('H'),
            floor($dateTime->format('i') / $minuteInterval) * $minuteInterval,
            0
        )->format("H:i");
    }

    /**
     * @param string $status
     * @return array|bool
     */
    public function getStatus(string $status)
    {
        $status = explode('/', $status);

        if (count($status) != 2) {
            return false;
        }
        if (!ctype_digit($status[1])) {
            return false;
        }

        $isHit  = strpos(strtolower($status[0]), 'hit') !== false;
        $isMiss = strpos(strtolower($status[0]), 'miss') !== false;
        if (!$isHit && !$isMiss) {
            return false;
        }

        return ['tcp' => $isHit ? 'hit' : 'miss' , 'code' => $status[1]];
    }
}