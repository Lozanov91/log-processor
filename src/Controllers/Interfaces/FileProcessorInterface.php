<?php
namespace Controllers\Interfaces;

interface FileProcessorInterface
{
    const DATE_POS = 0;
    const TIME_POS = 1;
    const STATUS_POS = 7;
    const BYTES_POS = 8;
    const HOST_POS = 10;
}