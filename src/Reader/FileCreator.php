<?php
namespace Reader;

use Exception;

class FileCreator
{
    /**
     * @param string $fileName
     * @return string
     * @throws Exception
     */
    public function validateFile(string $fileName)
    {
        $fullPath = $fileName;

        if (filesize($fullPath) == 0) {
            throw new Exception('File is empty.');
        }

        return $fullPath;
    }

    /**
     * @param $fileName
     * @param string $mode
     * @return string
     */
    public function createErrorFile(string $fileName, $mode = "wb")
    {
        $errorLogPath = pathinfo($fileName, PATHINFO_FILENAME).'.'.pathinfo($fileName, PATHINFO_EXTENSION).'.err';

        $this->unlinkExistingFile($errorLogPath);
        $this->create($errorLogPath, $mode);

        return $errorLogPath;
    }

    /**
     * @param $fileName
     * @param string $mode
     * @return string
     */
    public function createOutputFile(string $fileName, $mode = "wb")
    {
        $outputFilePath = pathinfo($fileName, PATHINFO_FILENAME).'.json';

        $this->unlinkExistingFile($outputFilePath);
        $this->create($outputFilePath , $mode);

        return $outputFilePath;
    }

    /**
     * @param $path
     */
    public function unlinkExistingFile(string $path)
    {
        if (file_exists($path)) {
            unlink($path);
        }
    }

    /**
     * @param $path
     * @param $mode
     */
    public function create(string $path, string $mode)
    {
        fopen($path, $mode);
    }
}