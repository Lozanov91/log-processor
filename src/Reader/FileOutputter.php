<?php
namespace Reader;

class FileOutputter
{
    /**
     * @param string $errorFile
     * @param array $errors
     */
    public function setErrorFile(string $errorFile, array $errors)
    {
        $handle = fopen($errorFile, "w");

        fwrite($handle, implode("\r\n", $errors));
        fclose($handle);
    }

    /**
     * @param string $outputFile
     * @param array $output
     */
    public function setOutputFile(string $outputFile, array $output)
    {
        $handle = fopen($outputFile, "w");

        fwrite($handle, json_encode($output));
        fclose($handle);
    }
}