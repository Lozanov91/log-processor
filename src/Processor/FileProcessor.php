<?php
namespace Processor;

use Exception;

class FileProcessor
{
    private $file;

    /**
     * FileProcessor constructor.
     * @param string $filename
     * @param string $mode
     * @throws Exception
     */
    function __construct(string $filename, string $mode)
    {
        if (!file_exists($filename)) {
            throw new Exception("Processed file not found.");
        }

        $this->file = fopen($filename, $mode);
    }

    /**
     * @return \Generator
     */
    public function iterateLines()
    {
        $read = $this->file;
        while (!feof($read)) {
            yield str_getcsv(fgets($read), ' ');
        }
        fclose($read);
    }
}