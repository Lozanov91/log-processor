<?php
error_reporting(E_ALL);
require __DIR__ . '/vendor/autoload.php';

use Controllers\FileProcessorController;
use Reader\FileCreator;
use Reader\FileOutputter;

if (!$argv || !isset($argv[1])) {
    echo 'You missed to specify filepath. Run again.'.PHP_EOL;
    exit;
}

$creator    = new FileCreator();
$outPutter  = new FileOutputter();
$controller = new FileProcessorController($creator, $outPutter);

$controller->processFile($argv[1]);

