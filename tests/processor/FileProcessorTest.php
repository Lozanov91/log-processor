<?php

use Processor\FileProcessor;

use PHPUnit\Framework\TestCase;

class FileProcessorTest extends TestCase
{
    private $testFile;
    private $testMode;

    public function setUp(): void
    {
        $this->testFile = __DIR__.DIRECTORY_SEPARATOR.'test-file.txt';
        $this->testMode = 'wb';
    }

    public function testFileProcessorThrowException()
    {
        $this->expectException(Exception::class);
        $processor = new FileProcessor($this->testFile, $this->testMode);
    }
}