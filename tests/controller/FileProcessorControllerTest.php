<?php

use PHPUnit\Framework\TestCase;

use Controllers\FileProcessorController;
use Reader\FileCreator;
use Reader\FileOutputter;

class FileProcessorControllerTest extends TestCase
{
    private $controller;

    public function setUp() : void
    {
        $this->controller = new FileProcessorController(new FileCreator(), new FileOutputter());
    }

    public function testControllerIsSet()
    {
        $this->assertObjectHasAttribute('fileCreator', $this->controller);
        $this->assertObjectHasAttribute('fileOutputter', $this->controller);
        $this->assertInstanceOf(FileProcessorController::class, $this->controller);
    }

    public function testControllerProcessFileException()
    {
        $notFile = '';

        $this->expectException(Exception::class);
        $this->controller->processFile($notFile);
    }

    public function testControllerProcessFileSourceFileProperty()
    {
        $stub = $this->getMockBuilder(FileCreator::class)
                     ->disableOriginalConstructor()
                     ->disableOriginalClone()
                     ->disableArgumentCloning()
                     ->disallowMockingUnknownTypes()
                     ->getMock();

        $stub->method('validateFile')->willReturn(__DIR__.DIRECTORY_SEPARATOR.'test-file.txt');

        $this->assertSame(__DIR__.DIRECTORY_SEPARATOR.'test-file.txt', $stub->validateFile('', ''));
    }

    public function testLineEmpty()
    {
        $line = [];
        $this->assertEmpty($line);
    }

    public function testLineMatchExpectedElementsCount()
    {
        $expected = [1, 2, 3, 4, '511', 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, ''];
        $actual   = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18];

        $this->assertCount(count($expected), $actual);
    }

    public function testLineHasCorrectDateAttribute()
    {
        $datePos = 1;
        $line    = ['some', '2014-04-02', '02:42', 'another', 'etc'];

        $this->assertArrayHasKey($datePos, $line);
        $this->assertNotFalse(strtotime($line[$datePos]));
        $this->assertEquals('2014-04-02', $line[$datePos]);
    }

    public function testLineHasCorrectTimeFormat()
    {
        $timePos = 2;
        $line    = ['some', '2014-04-02', '02:27', 'another', 'etc'];

        $this->assertArrayHasKey($timePos, $line);
        $this->assertNotFalse(strtotime($line[$timePos]));
        $this->assertEquals('02:20', $this->controller->roundDownToMinuteInterval(new \DateTime($line[$timePos])));
    }

    public function testLineHasCorrectStatus()
    {
        $statusPos = 8;
        $line      = ['some', '2014-04-02', '02:27', 'another', 'etc', 1442, 0, '-', 'TCP_HIT/200'];
        $line2     = ['some2', '2014-04-02', '02:29', 'another2', 'etc', 1914, 1, '=', 'TCP_MISS/400'];

        $this->assertArrayHasKey($statusPos, $line);
        $this->assertIsString($line[$statusPos]);
        $this->assertStringContainsString('HIT', $line[$statusPos]);
        $this->assertStringContainsString('MISS', $line2[$statusPos]);

        $status    = $this->controller->getStatus($line[$statusPos]);

        $this->assertCount(2, $status);
        $this->assertArrayHasKey('tcp', $status);
        $this->assertArrayHasKey('code', $status);
    }

    public function testLineHasCorrectBytes()
    {
        $bytesPos = 5;
        $line     = ['some', '2014-04-02', '02:27', 'another', 'etc', 13013, 0, '-', 'TCP_HIT/200'];

        $this->assertArrayHasKey($bytesPos, $line);
        $this->assertIsNumeric($line[$bytesPos]);
    }

    public function testLineHasCorrectHost()
    {
        $hostPos = 9;
        $line    = ['some', '2014-04-02', '02:27', 'another', 'etc', 13013, 0, '-', 'TCP_HIT/200', 'http://images.chem.co.uk/SMA-Wysoy-Soya-Infant-Formula-192594.jpg?o=gL1biz9dm5vMSjIVW9Npa@nLNCIj&V=iIxj&h=97&w=97&q=80'];

        $this->assertArrayHasKey($hostPos, $line);

        $host = parse_url($line[$hostPos]);

        $this->assertArrayHasKey('host', $host);
        $this->assertIsString($host['host']);
    }
}