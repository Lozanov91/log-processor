<?php

use Reader\FileCreator;
use Reader\FileOutputter;

use PHPUnit\Framework\TestCase;

class FileCreatorTest extends TestCase
{
    private $creator;
    private $outPutter;

    public function setUp() : void
    {
        $this->creator = new FileCreator();
        $this->outPutter = new FileOutputter();
    }

    public function testValidateFile()
    {
        $actual = '/var/www/html/logprocessor/log-processor.log';

        $this->assertFileExists($actual);
    }

    public function testCreateErrorFile()
    {
        $actual = $this->creator->createErrorFile('test-error-file.err');

        $this->assertIsString($actual);
        $this->assertFileExists($actual);
        $this->assertFileIsReadable($actual);
        $this->assertFileIsWritable($actual);

        $this->creator->unlinkExistingFile($actual);
    }

    public function testCreateOutputFile()
    {
        $actual = $this->creator->createOutputFile('test-output-file.json');

        $this->assertIsString($actual);
        $this->assertFileExists($actual);
        $this->assertFileIsReadable($actual);
        $this->assertFileIsWritable($actual);

        $this->creator->unlinkExistingFile($actual);
    }

    public function testCreateFile()
    {
        $path = __DIR__.DIRECTORY_SEPARATOR.'test-file.txt';
        $mode = 'wb';

        $this->creator->create($path, $mode);

        $this->assertIsString($path);
        $this->assertFileExists($path);
        $this->assertFileIsReadable($path);
        $this->assertFileIsWritable($path);

        $this->creator->unlinkExistingFile($path);
    }

    public function testSetErrorFile()
    {
        $testFile = $this->creator->createErrorFile('test-error-file.err');
        $expected = $this->creator->createErrorFile('test-error-file-2.json');

        $data = ['error1', 'error2', 'errorN'];

        $this->outPutter->setErrorFile($testFile, $data);
        $this->outPutter->setErrorFile($expected, $data);

        $this->assertFileExists($testFile);
        $this->assertFileExists($expected);

        $this->assertFileIsReadable($testFile);
        $this->assertFileIsReadable($expected);

        $this->assertFileIsWritable($testFile);
        $this->assertFileIsWritable($expected);

        $this->assertFileEquals($expected, $testFile);

        $this->creator->unlinkExistingFile($testFile);
        $this->creator->unlinkExistingFile($expected);
    }
}